package com.hcodez.protein;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void testClickImg1(View view) {
        Intent intent = new Intent(MainActivity.this, ExercisesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("categoryClicked", 1);
        startActivity(intent);
    }

    public void testClickImg2(View view) {
        Intent intent = new Intent(MainActivity.this, ExercisesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("categoryClicked", 2);
        startActivity(intent);
    }

    public void testClickImg3(View view) {
        Intent intent = new Intent(MainActivity.this, ExercisesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("categoryClicked", 3);
        startActivity(intent);
    }

    public void testClickImg4(View view) {
        Intent intent = new Intent(MainActivity.this, ExercisesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("categoryClicked", 4);
        startActivity(intent);
    }
}