package com.hcodez.protein;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hcodez.protein.Users.User;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private Button btn_register;
    private EditText etxt_email;
    private EditText etxt_password;

    private DatabaseReference mDatabase;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initializeUI();
    }

    private void initializeUI() {
        etxt_email = findViewById(R.id.etxt_email);
        etxt_password = findViewById(R.id.etxt_password);
        btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etxt_email.getText().toString().trim();
                String pass = etxt_password.getText().toString().trim();

                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)) {
                    callCustomSnackbar("All fields must be filled!", "Gotcha!");
                } else if (pass.length() < 6) {
                    callCustomSnackbar("Password has to have 6 or more characters!", "Understood!");
                } else {
                    register(email, pass);
                }
            }
        });

        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void register (final String email, final String pass) {
        auth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            Map<String, String> newUser = new HashMap<>();
                            newUser.put("id", userid);
                            newUser.put("email", email);
                            newUser.put("password", pass);

                            mDatabase.child("users").child(userid).setValue(newUser)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                        } else {
                            Toast.makeText(RegisterActivity.this, "We couldn't register You with that email!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void callCustomSnackbar(String messageText, String buttonText) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, messageText, Snackbar.LENGTH_LONG)
                .setAction(buttonText, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) { }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }
}