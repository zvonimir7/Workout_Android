package com.hcodez.protein.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hcodez.protein.MainActivity;
import com.hcodez.protein.R;
import com.hcodez.protein.SpecificExerciseActivity;
import com.hcodez.protein.StartActivity;

import java.util.List;

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {

    private Context mContext;
    private List<String> mExercises;
    private boolean IsExerciseHolder;

    public ExerciseAdapter(Context mContext, List<String> mExercises, boolean IsExerciseHolder) {
        this.mContext = mContext;
        this.mExercises = mExercises;
        this.IsExerciseHolder = IsExerciseHolder;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_exercise, parent, false);
        return new ExerciseAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder,final int position) {
        holder.title.setText(mExercises.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsExerciseHolder) {
                    Intent intent = new Intent(mContext, SpecificExerciseActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("exerciseClicked", mExercises.get(position));
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mExercises.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.txt_title);
        }
    }
}
