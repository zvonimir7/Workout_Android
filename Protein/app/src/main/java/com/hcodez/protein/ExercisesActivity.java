package com.hcodez.protein;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.hcodez.protein.Adapters.ViewPagerAdapter;
import com.hcodez.protein.FragmentsMain.ArmsFragment;
import com.hcodez.protein.FragmentsMain.ChestsFragment;
import com.hcodez.protein.FragmentsMain.LegsFragment;
import com.hcodez.protein.FragmentsMain.ShouldersFragment;

public class ExercisesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);

        initializeUI();
    }

    private void initializeUI() {
        ViewPager viewPager = findViewById(R.id.vp_exercises);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        updateUIWithFragments(viewPagerAdapter);
        viewPager.setAdapter(viewPagerAdapter);
    }

    private void updateUIWithFragments(ViewPagerAdapter viewPagerAdapter) {
        int orderFragmentPriority = getIntent().getIntExtra("categoryClicked", 1);

        if (orderFragmentPriority == 1){
            viewPagerAdapter.addFragment(new ChestsFragment());
            viewPagerAdapter.addFragment(new LegsFragment());
            viewPagerAdapter.addFragment(new ArmsFragment());
            viewPagerAdapter.addFragment(new ShouldersFragment());
        } else if (orderFragmentPriority == 2) {
            viewPagerAdapter.addFragment(new LegsFragment());
            viewPagerAdapter.addFragment(new ChestsFragment());
            viewPagerAdapter.addFragment(new ArmsFragment());
            viewPagerAdapter.addFragment(new ShouldersFragment());
        } else if (orderFragmentPriority == 3) {
            viewPagerAdapter.addFragment(new ArmsFragment());
            viewPagerAdapter.addFragment(new ChestsFragment());
            viewPagerAdapter.addFragment(new LegsFragment());
            viewPagerAdapter.addFragment(new ShouldersFragment());
        }  else {
            viewPagerAdapter.addFragment(new ShouldersFragment());
            viewPagerAdapter.addFragment(new ChestsFragment());
            viewPagerAdapter.addFragment(new LegsFragment());
            viewPagerAdapter.addFragment(new ArmsFragment());
        }
    }
}