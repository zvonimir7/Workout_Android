package com.hcodez.protein;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

public class SpecificExerciseActivity extends AppCompatActivity {

    private TextView txt_exerciseTitle;
    private Button btn_stopWatch;
    private Chronometer chr_stopWatch;
    private long pauseOffset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_exercise);

        initializeUI();
    }

    private void initializeUI() {
        txt_exerciseTitle = findViewById(R.id.txt_exerciseTitle);
        txt_exerciseTitle.setText(getIntent().getStringExtra("exerciseClicked"));

        btn_stopWatch = findViewById(R.id.btn_stopWatch);
        chr_stopWatch = findViewById(R.id.chr_stopWatch);
    }

    public void operateWithStopwatch(View view) {
        String currentStopwatchStatus = btn_stopWatch.getText().toString();
        if (currentStopwatchStatus.equals("START/RESUME")) {
            chr_stopWatch.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chr_stopWatch.start();
            btn_stopWatch.setText("PAUSE");
        } else {
            chr_stopWatch.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chr_stopWatch.getBase();
            btn_stopWatch.setText("START/RESUME");
        }
    }

    public void resetStopWatch(View view) {
        chr_stopWatch.stop();
        chr_stopWatch.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
        btn_stopWatch.setText("START/RESUME");
    }
}