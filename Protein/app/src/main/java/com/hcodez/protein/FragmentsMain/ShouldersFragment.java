package com.hcodez.protein.FragmentsMain;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcodez.protein.Adapters.ExerciseAdapter;
import com.hcodez.protein.R;

import java.util.ArrayList;
import java.util.List;

public class ShouldersFragment extends Fragment {

    private RecyclerView rv_chests_up;
    private RecyclerView rv_chests_down;
    private List<String> Exercises;
    private List<String> Descriptions;
    private ExerciseAdapter exerciseAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle bundle){
        View view = inflater.inflate(R.layout.fragment_shoulders, parent,false);

        setupRecyclers(view);
        return view;
    }

    private void setupRecyclers(View view) {
        rv_chests_up = view.findViewById(R.id.rv_chests_up);
        rv_chests_up.setHasFixedSize(true);
        rv_chests_up.setLayoutManager(new LinearLayoutManager(getContext()));

        rv_chests_down = view.findViewById(R.id.rv_chests_down);
        rv_chests_down.setHasFixedSize(true);
        rv_chests_down.setLayoutManager(new LinearLayoutManager(getContext()));

        Exercises = new ArrayList<>();
        Descriptions = new ArrayList<>();

        fetchExercisesFromDB();
        fetchExercisesDescFromDB();
    }

    private void fetchExercisesDescFromDB() {
        Query query = FirebaseDatabase.getInstance().getReference("exercises_desc").child("shoulders");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Descriptions.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String exercise_desc = snapshot.getValue(String.class);
                    assert exercise_desc != null;
                    Descriptions.add(exercise_desc);
                }
                exerciseAdapter = new ExerciseAdapter(getContext(), Descriptions, false);
                rv_chests_up.setAdapter(exerciseAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchExercisesFromDB() {
        Query query = FirebaseDatabase.getInstance().getReference("exercises").child("shoulders");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Exercises.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String exercise = snapshot.getValue(String.class);
                    assert exercise != null;
                    Exercises.add(exercise);
                }
                exerciseAdapter = new ExerciseAdapter(getContext(), Exercises, true);
                rv_chests_down.setAdapter(exerciseAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}